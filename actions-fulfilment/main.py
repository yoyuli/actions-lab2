import json
import requests


def get_cat_fact():
    # api-endpoint
    url = "https://cat-fact.herokuapp.com/facts/random"

    parameters = {
        "animal_type": "cat",
        "amount": 1
    }

    response = requests.get(url, params=parameters)
    response_json = response.json()
    print("Full response in JSON:")
    print(response_json)
    print("\n")

    fact = response_json['text']
    print("Fact: %s" % fact)
    return fact


def fulfill(request):
    request_json = request.get_json()

    print(json.dumps(request_json))

    intent = request_json['queryResult']['intent']['displayName']
    print('The intent is %s' % intent)

    if intent == "get_cat_fact":
        fulfillmentText = get_cat_fact()
    if intent == "ask-city":
        parameters = request_json['queryResult']['parameters']
        if parameters['geo-city'] is not None:
            fulfillmentText = 'Welcome to %s' % parameters['geo-city']
    else:
        fulfillmentText = "Can you say that again?"

    response_dict = {
        'fulfillmentText': fulfillmentText
    }
    response_string = json.dumps(response_dict)
    return response_string

